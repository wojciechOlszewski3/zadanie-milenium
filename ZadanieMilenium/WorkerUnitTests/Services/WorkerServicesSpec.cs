﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workers.Domain.Models;
using Workers.Domain.Repositories;
using Workers.Services.Dto;
using Workers.Services.Services;
using static TddXt.AnyRoot.Root;
namespace WorkerUnitTests.Services
{
   public  class WorkerServicesSpec
    {
        [Test]
        public void ShouldReturnWorkersDtoWhenGetWorkersAsyncInvoke()
        {
            var mockWorkerRepository = Substitute.For<IWorkerRepository>();
            var workers = Any.Instance<IEnumerable<Worker>>();
            mockWorkerRepository.GetWorkersAsync().Returns(workers);
            var workersService = new WorkerServices(mockWorkerRepository);

            var result = workersService.GetWorkersAsync().Result;

            Assert.AreEqual(workers.Count(), result.Count(),"number of workers is not equal");
            AssertWorkers(result, workers);
        }

        private void AssertWorkers(IEnumerable<WorkerDto> expected, IEnumerable<Worker> actual)
        {
            foreach (var workerDto in expected)
            {
                var actualWorker = actual.Single(w => w.Id == workerDto.Id);
                Assert.AreEqual(workerDto.Name, actualWorker.Name);
                Assert.AreEqual(workerDto.Surname, actualWorker.Surname);
            }
        }
    }
}
