﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Workers.Services.Dto;
using Workers.Services.Services;

namespace ZadanieMilenium.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WorkersController : ControllerBase
    {
        private readonly IWorkerServices _workerServices;

        public WorkersController(IWorkerServices workerServices)
        {
            _workerServices = workerServices;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _workerServices.GetWorkersAsync());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _workerServices.GetWorkerAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> Post(WorkerDto workerDto)
        {
            // warto byłoby opakować to w jakiś rezultat który pozwoliłby wyeliminować try i catch ogólnie w każdej metodzie kontrolera trzeba uwzględnić błędy. Trochę mało czasu żeby wszystko zrobić ;)
            try
            {
                await _workerServices.AddWorkerAsync(workerDto);
                return Ok();
            }
            catch 
            {
                return StatusCode(500);
            }
           
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update(WorkerDto workerDto)
        {
            await _workerServices.UpdateWorkerAsync(workerDto);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _workerServices.DeleteWorkerAsync(id);
            return Ok();
        }
    }
}
