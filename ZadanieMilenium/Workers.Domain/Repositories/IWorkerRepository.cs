﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Workers.Domain.Models;

namespace Workers.Domain.Repositories
{
   public interface IWorkerRepository
    {
        Task<Worker> GetWorkerAsync(int id);
        Task<IEnumerable<Worker>> GetWorkersAsync();

        Task AddWorkerAsync(Worker worker);

        Task UpdateWorkerAsync(Worker workter);

        Task DeleteWorkerAsync(int id);
    }
}
