﻿using Workers.Domain.Repositories;
using Workers.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Workers.Repository
{
   public class WorkerRepository : IWorkerRepository
    {
        private List<Worker> _workers = new List<Worker> {
        new Worker {Id=1 , Name="Test", Surname = "Test"}
        };

        public async Task AddWorkerAsync(Worker worker)
        {
            var lastWorkerId = _workers.Last()?.Id;
            if (lastWorkerId == null)
                worker.Id = 0;
            worker.Id = (int)lastWorkerId++;
            _workers.Add(worker);
        }

        public async Task DeleteWorkerAsync(int id)
        {
            var worker = _workers.SingleOrDefault(w => w.Id == id);
            if (worker == null)
                throw new NullReferenceException($"can not find user by id: { worker.Id}");

            _workers.Remove(worker);
        }

        public async Task<Worker> GetWorkerAsync(int id)
        {
            var worker = _workers.SingleOrDefault(w => w.Id == id);
            return await Task.FromResult(worker);
        }

        public async Task<IEnumerable<Worker>> GetWorkersAsync()
        => await Task.FromResult(_workers);
        
        public async Task UpdateWorkerAsync(Worker worker)
        {
            var workerToUpdate = _workers.SingleOrDefault(w => w.Id == worker.Id);

            if (workerToUpdate == null)
                throw new NullReferenceException($"can not find user by id: { worker.Id}");

            worker.Name = worker.Name;
            worker.Surname = worker.Surname;
        }
    }
}
