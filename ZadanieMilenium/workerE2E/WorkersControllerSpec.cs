﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using workerE2E.Extensions;
using Workers.Domain.Models;
using Workers.Domain.Repositories;
using ZadanieMilenium;
using static TddXt.AnyRoot.Root;

namespace workerE2E
{
    public class WorkersControllerSpec
    {
        [Test]
        public  void test()
        {
            var mockWorkerRepository = Substitute.For<IWorkerRepository>();
            var workers = Any.Instance<IEnumerable<Worker>>();
            mockWorkerRepository.GetWorkersAsync().Returns(workers);
            
            var factory = new CustomWebApplicationFactory<Startup>(services =>
            {
                services.SwapTransient(s=>mockWorkerRepository);
            });
            var client = factory.CreateClient();
            var responsJson = JsonSerializer.Serialize(workers, options);
            var response =  client.GetAsync("workers").Result;

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            AsserHttpResponseContent(responsJson, response.Content);
        }

        private void AsserHttpResponseContent(string expected, HttpContent content)
        {
            var result = content.ReadAsStringAsync().Result;
            Assert.AreEqual(expected, result);
        }

        private static JsonSerializerOptions options => new JsonSerializerOptions
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        };
    }
}
