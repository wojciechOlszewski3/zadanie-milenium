﻿using Microsoft.Extensions.DependencyInjection;
using Workers.Domain.Repositories;
using Workers.Repository;
using Workers.Services.Services;

namespace Workers.Services.Extensions
{
    public static class WorkerServicesExtensions
    {
        public static IServiceCollection AddWorkerService(this IServiceCollection services)
        {
            services.AddTransient<IWorkerRepository, WorkerRepository>();
            services.AddTransient<IWorkerServices, WorkerServices>();
            return services;

        }
    }
}
