﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workers.Domain.Models;
using Workers.Domain.Repositories;
using Workers.Services.Dto;

namespace Workers.Services.Services
{
    public interface IWorkerServices
    {
        Task<WorkerDto> GetWorkerAsync(int id);
        Task<IEnumerable<WorkerDto>> GetWorkersAsync();

        Task AddWorkerAsync(WorkerDto worker);

        Task UpdateWorkerAsync(WorkerDto workter);

        Task DeleteWorkerAsync(int id);
    }
    public class WorkerServices : IWorkerServices
    {
        private readonly IWorkerRepository _workerRepository;

        public WorkerServices(IWorkerRepository workerRepository)
        {
            _workerRepository = workerRepository;
        }
        public async Task AddWorkerAsync(WorkerDto workerDto)
        {
            var worker = GetWorkerFromWorkerDto(workerDto);
            await _workerRepository.AddWorkerAsync(worker);
        }

        public async Task DeleteWorkerAsync(int id)
        {
            await _workerRepository.DeleteWorkerAsync(id);
        }

        public async Task<WorkerDto> GetWorkerAsync(int id)
        {
            var worker = await _workerRepository.GetWorkerAsync(id);
            return new WorkerDto(worker);
        }

        public async Task<IEnumerable<WorkerDto>> GetWorkersAsync()
        {
            var workers = await _workerRepository.GetWorkersAsync();
            return workers.Select(w => new WorkerDto(w));
        }

        public async Task UpdateWorkerAsync(WorkerDto workterDto)
        {
            await _workerRepository.UpdateWorkerAsync(GetWorkerFromWorkerDto(workterDto));
        }

        private static Worker GetWorkerFromWorkerDto(WorkerDto workerDto)
        {
            return new Worker { Id = workerDto.Id, Name = workerDto.Name, Surname = workerDto.Surname };
        }
    }
}
