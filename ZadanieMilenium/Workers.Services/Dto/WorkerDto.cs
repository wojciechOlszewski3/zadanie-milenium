﻿using System;
using System.Collections.Generic;
using System.Text;
using Workers.Domain.Models;

namespace Workers.Services.Dto
{
   public class WorkerDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string  Surname { get; set; }

        public WorkerDto(Worker worker)
        {
            Id = worker.Id;
            Name = worker.Name;
            Surname = worker.Surname;
        }
    }
}
